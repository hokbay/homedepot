import fetch from 'node-fetch';
import { CrawlerApiPMap, WpApiPMap } from './DataTypes';
import { HdRawDataParser } from './HdRawDataParser';
import { HdProductParser } from './HdProductParser';

export class HdProductSearchExtractor {
    private _args: CrawlerApiPMap;

    constructor(args: CrawlerApiPMap) {
        this._args = args;
    }

    async callCrawlerApi(): Promise<string> {
        const url = this._args['crawler_api_url'] as string;
        const options = {
            method: 'POST',
            mode: 'cors',
            headers: {
                Token: this._args['crawler_api_token'] as string,
            },
            body: JSON.stringify({
                inputs: {
                    category_id: '',
                    keyword: (this._args['keyword'] as string).replace('_', ' '),
                    store_id: '',
                    in_store_filter: '',
                    api_key: '',
                    page_size: this._args['page_size'].toString(),
                    start_index: this._args['startIndex'].toString(),
                },
                proxy: {
                    type: this._args['proxy_type'],
                    location: this._args['proxy_location'],
                },
                format: 'json',
            }),
        };
        try {
            const response = await fetch(url, options);
            const responseJson = await response.json();
            return JSON.stringify(responseJson);
        } catch (err) {
            console.log('CrawlerAPI ERROR - ', err);
            return err;
        }
    }

    async extractData(wpApiParamMap: WpApiPMap) {
        let totalProductCount: number = 0;
        let startIndex: number = 0;
        let pageNumber: number = 1; // Current page processing
        let pageSize: number = 0; // Number of products per page
        this._args['startIndex'] = '';

        let responseJson = await this.callCrawlerApi();
        const responseJsonObj = JSON.parse(responseJson);
        // console.log('resposeJsonObj= ', responseJsonObj['object']['response']);

        totalProductCount = responseJsonObj['object']['response']['response_json']['searchReport']['totalProducts'];

        console.log('Total number of products available - ', totalProductCount);

        if (totalProductCount > this._args['max_count']) totalProductCount = this._args['max_count'] as number;

        console.log('Total number of products to be extracted - ', totalProductCount);
        console.log('Each time will process number of products - ', this._args['page_size']);

        pageSize = this._args['page_size'] as number;

        let leftOver = totalProductCount % pageSize;
        let totalPages = Math.floor(totalProductCount / pageSize);

        if (leftOver > 0) totalPages += 1;

        console.log('Total process times - ', totalPages);

        const wooCommerceParser = new HdProductParser();
        const rawDataParser = new HdRawDataParser();
        let loop = setInterval(() => {
            // Process 1 page per loop.
            startIndex = pageSize * (pageNumber - 1);

            this._args['startIndex'] = startIndex.toString();

            // Not using await here so can do next loop concurrently.
            this.callCrawlerApi().then((responseJson) => {
                rawDataParser.parseData(responseJson);
                wooCommerceParser.parseData(responseJson, this._args['data_source'] as string, wpApiParamMap);
            });

            pageNumber = pageNumber + 1;
            if (pageNumber > totalPages) {
                clearInterval(loop);
                console.log('Product data extracted and updated Completed.');
                console.log('-- Pages processed: ', pageNumber - 1);
            }
        }, 1000); // API limit = 1 call per second.

        /* *** Debug Code *** */
        /*
        with open('raw_data.json', 'w') as file:
            json.dump(responseJson, file);
        """*/
        //}
    }
}
