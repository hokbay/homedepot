export class ExtractorArgs {
    private _id: number;
    private _dataSource: string;
    private _keyword: string;
    private _maxCount: string;
    private _pageSize: number;
    private _prefix: string;
    private _suffix: string;
    private _crawlerApiUrl: string;
    private _crawlerApiToken: string;
    private _proxyType: string;
    private _proxyLocation: string;
    private _status: string;

    constructor() {
        this._id = 0;
        this._dataSource = '';
        this._keyword = '';
        this._maxCount = '';
        this._pageSize = 48;
        this._prefix = '';
        this._suffix = '';
        this._crawlerApiUrl = '';
        this._crawlerApiToken = '';
        this._proxyType = '';
        this._proxyLocation = '';
        this._status = '';
    }

    set id(id: number) {
        this._id = id;
    }

    set dataSource(dataSource: string) {
        this._dataSource = dataSource;
    }
    set keyword(keyword: string) {
        this._keyword = keyword;
    }
    set maxCount(maxCount: string) {
        this._maxCount = maxCount;
    }
    set pageSize(pageSize: number) {
        this._pageSize = pageSize;
    }
    set prefix(prefix: string) {
        this._prefix = prefix;
    }
    set suffix(suffix: string) {
        this._suffix = suffix;
    }
    set crawlerApiUrl(crawlerApiUrl: string) {
        this._crawlerApiUrl = crawlerApiUrl;
    }
    set crawlerApiToken(crawlerApiToken: string) {
        this._crawlerApiToken = crawlerApiToken;
    }
    set proxyType(proxyType: string) {
        this._proxyType = proxyType;
    }
    set proxyLocation(proxyLocation: string) {
        this._proxyLocation = proxyLocation;
    }
    set status(status: string) {
        this._status = status;
    }
    get id(): number {
        return this._id;
    }
    get dataSource(): string {
        return this._dataSource;
    }
    get keyword(): string {
        return this._keyword;
    }
    get maxCount(): string {
        return this._maxCount;
    }
    get pageSize(): number {
        return this._pageSize;
    }
    get prefix(): string {
        return this._prefix;
    }
    get suffix(): string {
        return this._suffix;
    }
    get crawlerApiUrl(): string {
        return this._crawlerApiUrl;
    }
    get crawlerApiToken(): string {
        return this._crawlerApiToken;
    }
    get proxyType(): string {
        return this._proxyType;
    }
    get proxyLocation(): string {
        return this._proxyLocation;
    }
    get status(): string {
        return this._status;
    }
}
