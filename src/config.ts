import * as config from './config.json';

export class Config {
    private static _sqlDb = {
        host: '',
        user: '',
        password: '',
        database: '',
    };

    private static _noSqlDb = {
        ancestorPath: [''],
        kind: '',
    };

    static get sqlDb() {
        //console.log('ENV = ', process.env);
        console.log('NODE_ENV = ', process.env.NODE_ENV);
        if (process.env.NODE_ENV === 'development') {
            return {
                host: config.development.sqlHost,
                user: config.development.sqlUsername,
                password: config.development.sqlPassword,
                database: config.development.sqlDatabase,
            };
        } else if (process.env.NODE_ENV === 'production') {
            return {
                host: config.production.sqlHost,
                socketPath: config.production.sqlSocketPath,
                user: config.production.sqlUsername,
                password: config.production.sqlPassword,
                database: config.production.sqlDatabase,
            };
        }
        return Config._sqlDb;
    }

    static get noSqlDb() {
        Config._noSqlDb.ancestorPath = config.noSqlPath.ancestorPath;
        Config._noSqlDb.kind = config.noSqlPath.kind;
        return Config._noSqlDb;
    }
}
