import { ExtractorArgs } from './ExtractorArgs';
import { WpProduct } from './WpProduct';
import { Json, WpApiPMap, CrawlerApiPMap } from './DataTypes';
import { WpApiHandler } from './WpApiHandler';
import { NONAME } from 'dns';

function prepareCrawlerApiParamMap(argObj: ExtractorArgs): CrawlerApiPMap {
    let pMap = {} as CrawlerApiPMap;
    pMap.id = argObj.id;
    pMap.data_source = argObj.dataSource;
    return pMap;
}

export class HdProductParser {
    private _createBatchUpdateJson(createArray: WpProduct[], updateArray: WpProduct[]) {
        const data = {
            create: createArray,
            update: updateArray,
        };
        return data;
    }

    private _setProductImages(imageUrl: string) {
        return [{ src: imageUrl }];
    }

    async parseData(rawDataJson: string, dataSource: string, wpApiParamMap: WpApiPMap) {
        let updatePool = [];
        let createPool = [];
        const productJsonObj = JSON.parse(rawDataJson);
        const skuPrefix = 'home_depot_';

        const productType = 'external';
        const publish = '1';
        const visibility = 'visible';
        const space = ' ';
        const buttonText = 'Go to buy';
        const brandStr = 'Brand: ';
        const modelStr = 'Model: ';
        const priceStr = 'Price: ';
        const reviewStr = 'Review: ';
        let categoryHashMap: { [index: string]: string } = { outdoors: 'Lawn & Garden' }; //category map from external

        const productCount = productJsonObj['object']['response']['response_json']['skus'].length;
        const products = productJsonObj['object']['response']['response_json']['skus'];

        if (productCount > 0) {
            let wpApi = new WpApiHandler(wpApiParamMap);
            for (let i = 0; i < productCount; i++) {
                let sku = products[i]['productId'].toString();
                let productUrl = wpApiParamMap.productUrlPrefix + products[i]['productUrl'].toString();
                let brandName: string;
                let modelNumber: string;

                if ('brandName' in products[i]['info']) brandName = products[i]['info']['brandName'].toString();
                else brandName = '';

                let productLabel = products[i]['info']['productLabel'].toString();
                if (products[i]['info'].hasOwnProperty('modelNumber'))
                    modelNumber = products[i]['info']['modelNumber'].toString();
                else modelNumber = '';

                let category = products[i]['info']['categoryHierarchy'][0].toString();

                if (categoryHashMap.hasOwnProperty(category)) category = categoryHashMap[category].toLowerCase();

                let imageUrl = products[i]['info']['imageUrl'].replace('<SIZE>', '1000').toString();
                let originalPrice = products[i]['storeSku']['pricing']['originalPrice'].toString();
                let specialPrice = products[i]['storeSku']['pricing']['specialPrice'].toString();
                let ratingsReviews = products[i]['ratingsReviews']['averageRating'].toString();
                let totalReviews = products[i]['ratingsReviews']['totalReviews'].toString();
                let description =
                    brandStr +
                    brandName +
                    '\n' +
                    modelStr +
                    modelNumber +
                    '\n' +
                    priceStr +
                    specialPrice +
                    '\n' +
                    reviewStr +
                    ratingsReviews +
                    ' of total ' +
                    totalReviews +
                    ' reviews' +
                    '\n';

                sku = skuPrefix + sku;

                let productId = await wpApi.checkIfProductExist(sku);

                let product: WpProduct = {
                    sku: sku,
                    name: brandName + space + productLabel,
                    type: productType,
                    regular_price: originalPrice,
                    sale_price: specialPrice,
                    external_url: productUrl,
                    button_text: buttonText,
                    description: description,
                    categories: await wpApi.getProductCategories(category),
                    images: this._setProductImages(imageUrl),
                };
                if (productId !== undefined) {
                    product.id = productId;
                    updatePool.push(product);
                } else {
                    createPool.push(product);
                }
            }
            const batchUpdateJosn = this._createBatchUpdateJson(createPool, updatePool);

            wpApi.batchUpdateProducts(batchUpdateJosn);
        }
    }
}
