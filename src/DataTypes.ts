export type Json = { [key: string]: unknown };
export type CrawlerApiPMap = {
    [key: string]: string | number;
    id: number;
    dataSource: string;
    keyword: string;
    maxCount: string;
    pageSize: number;
    prefix: string;
    suffix: string;
    crawlerApiUrl: string;
    crawlerApiToken: string;
    proxyType: string;
    proxyLocation: string;
    status: string;
};
export type WpApiPMap = {
    [key: string]: string;
    wp_batch_update_product_api_url: string;
    wp_api_key: string;
    wp_api_secret: string;
};
