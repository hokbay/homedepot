import { HdProductSearchExtractor } from './HdProductSearchExtractor';
import { ExtractorArgs } from './ExtractorArgs';
import { CrawlerApiPMap, WpApiPMap, Json } from './DataTypes';
import * as mysql from 'mysql';
import { Config } from './config';

function prepareCrawlerApiParamMap(argObj: ExtractorArgs): CrawlerApiPMap {
    let CrawlerApiPMap = {} as CrawlerApiPMap;
    CrawlerApiPMap.id = argObj.id;
    CrawlerApiPMap.data_source = argObj.dataSource;
    CrawlerApiPMap['keyword'] = argObj.keyword;
    CrawlerApiPMap['max_count'] = argObj.maxCount;
    CrawlerApiPMap['page_size'] = argObj.pageSize;
    CrawlerApiPMap['prefix'] = argObj.prefix;
    CrawlerApiPMap['suffix'] = argObj.suffix;
    CrawlerApiPMap['crawler_api_url'] = argObj.crawlerApiUrl;
    CrawlerApiPMap['crawler_api_token'] = argObj.crawlerApiToken;
    CrawlerApiPMap['proxy_type'] = argObj.proxyType;
    CrawlerApiPMap['proxy_location'] = argObj.proxyLocation;
    CrawlerApiPMap['status'] = argObj.status;
    return CrawlerApiPMap;
}

function prepareWpApiParamMap(dbParamTable: Json[]): WpApiPMap {
    let pMap = {} as WpApiPMap;
    for (let row of dbParamTable) {
        pMap[row.name as string] = row.value as string;
    }
    return pMap;
}

function mysqlQuery(connection: mysql.Connection, sql: string) {
    return new Promise((resolve, reject) => {
        connection.query(sql, (err, rows) => {
            if (err) return reject(err);
            resolve(rows);
        });
    });
}

export async function homeDepotExtractor(request: unknown) {
    const GET_PENDING_ARGS = "SELECT * FROM extractor_args where status='pending' order by data_source LIMIT 5";
    const GET_PRODUCT_UPLOAD_ARGS = 'SELECT * FROM product_upload_args';
    let dbConnection = await mysql.createConnection(Config.sqlDb);
    //get product api args
    const productUploadArgsResults = (await mysqlQuery(dbConnection, GET_PRODUCT_UPLOAD_ARGS)) as Json[];

    //make api args as map
    const wpApiParamMap = prepareWpApiParamMap(productUploadArgsResults);

    //get extractor args
    const results = await mysqlQuery(dbConnection, GET_PENDING_ARGS);
    //let results = await mysqlQuery(dbConnection, 'SELECT * FROM extractor_args'); //debug code
    //console.log('rows=: ', results); //debug code
    const args = new ExtractorArgs();

    //update status
    for (let row of results as []) {
        const SET_PROCESSING_STATUS = "UPDATE extractor_args SET status='processing' WHERE arg_id = " + row['arg_id'];
        dbConnection.query(SET_PROCESSING_STATUS);
        dbConnection.commit();

        args.id = row['arg_id'];
        args.dataSource = row['data_source'];
        args.keyword = row['keyword'];
        args.maxCount = row['max_count'];
        args.pageSize = row['page_size'];
        args.prefix = row['prefix'];
        args.suffix = row['suffix'];
        args.crawlerApiUrl = row['crawler_api_url'];
        args.crawlerApiToken = row['crawler_api_token'];
        args.proxyType = row['proxy_type'];
        args.proxyLocation = row['proxy_location'];
        args.status = row['status'];

        const GET_PRODUCT_PREFIX =
            "SELECT product_url_prefix FROM product_url_map WHERE data_source = '" + args.dataSource + "'";
        const productUrlMapRow = (await mysqlQuery(dbConnection, GET_PRODUCT_PREFIX)) as [Json];
        wpApiParamMap.productUrlPrefix = productUrlMapRow[0]['product_url_prefix'] as string;

        const param_map = prepareCrawlerApiParamMap(args);

        const extractor = new HdProductSearchExtractor(param_map);
        console.log('start extracting...');
        extractor.extractData(wpApiParamMap);
        dbConnection.query("UPDATE extractor_args SET status='extracted' WHERE arg_id = " + row['arg_id']);
        dbConnection.commit();
    }
    dbConnection.end();
}

if (require.main === module) {
    homeDepotExtractor('');
}
