import { Json } from './DataTypes';

export interface WpProduct {
    // Do NOT change naming convention for it must comply with Wordpress REST API.
    id?: number;
    sku: string;
    name: string;
    type: string;
    regular_price: string;
    sale_price: string;
    external_url: string;
    button_text: string;
    description: string;
    categories: Json[];
    images: Json[];
}
