import { Datastore } from '@google-cloud/datastore';
import { Json } from './DataTypes';

export namespace GCloudHandler {
    export class DataStoreHandler {
        private _datastore: Datastore;

        constructor() {
            this._datastore = new Datastore();
        }

        _getKey_(keyString: string, kind = 'default', ancestorPath?: string[]) {
            if (ancestorPath != undefined) {
                return this._datastore.key([...ancestorPath, kind, keyString]);
            } else {
                return this._datastore.key([kind, keyString]);
            }
        }

        _createBatch_(productKeyPairs: Json[], kind = 'default', ancestorPath?: string[]) {
            const entities = productKeyPairs.reduce((entities: Json[], pair: Json) => {
                const objKey = Object.keys(pair)[0];
                entities.push({
                    key: this._getKey_(objKey, kind, ancestorPath),
                    data: pair[objKey],
                });
                return entities;
            }, []);

            return entities;
        }

        public write(productKeyPairs: Json[], kind = 'default', ancestorPath?: string[]) {
            if (productKeyPairs.length === 1) {
                const objKey = Object.keys(productKeyPairs[0])[0];
                const dbKey = this._getKey_(objKey, kind, ancestorPath);
                const entity = {
                    key: dbKey,
                    data: productKeyPairs[0].objKey,
                };
                this._datastore.upsert(entity);
            } else {
                const entities = this._createBatch_(productKeyPairs, kind, ancestorPath);
                this._datastore.upsert(entities);
            }
        }
    }
}
