import { GCloudHandler } from './GCloudHandler';
import util = require('util');
import { Json } from './DataTypes';
import { Config } from './config';

export class HdRawDataParser {
    private _writeToDatabase(productKeyPairs: Json[]) {
        let dataStore = new GCloudHandler.DataStoreHandler();
        dataStore.write(productKeyPairs, Config.noSqlDb.kind, Config.noSqlDb.ancestorPath);
    }

    parseData(rawDataJson: string) {
        const rawDataJsonObj = JSON.parse(rawDataJson);
        //console.log(util.inspect(rawDataJsonObj, { depth: 12 }));
        const searchKeyWord = rawDataJsonObj['object']['inputs']['keyword'];
        const productCount = rawDataJsonObj['object']['response']['response_json']['skus'].length;
        const products = rawDataJsonObj['object']['response']['response_json']['skus'];

        if (productCount > 0) {
            const productKeyPairs = products.reduce((productKeyPairs: Json[], product: Json) => {
                const timeStamp = new Date().toISOString();
                product['keyword'] = searchKeyWord;
                const key = product['productId'] + '_' + timeStamp;
                productKeyPairs.push({ [key]: product });
                return productKeyPairs;
            }, []);
            this._writeToDatabase(productKeyPairs);
        }
    }
}
