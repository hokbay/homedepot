import { Json, WpApiPMap } from './DataTypes';
import { WpProduct } from './WpProduct';
import fetch from 'node-fetch';
import { Cipher } from 'crypto';
import { METHODS } from 'http';

enum HttpRequestMethod {
    post = 'POST',
    get = 'GET',
}

export class WpApiHandler {
    private _pMap: WpApiPMap;
    private _headers: Record<string, string>;

    constructor(pMap: WpApiPMap) {
        let key = pMap['wp_api_key'] + ':' + pMap['wp_api_secret'];
        key = Buffer.from(key).toString('base64');
        this._pMap = pMap;
        this._headers = {
            'Content-Type': 'application/json',
            Authorization: 'Basic ' + key,
        };
    }

    private async _callWpApi(endpoint: string, requestMethod: HttpRequestMethod, data: string = '') {
        //console.log('endpoint = ', endpoint); // Debug
        //console.log('headers = ', this._headers); //Debug
        //console.log('data = ', data); //Debug
        try {
            let res;
            if (requestMethod === HttpRequestMethod.post)
                res = await fetch(endpoint, { method: 'POST', headers: this._headers, body: data });
            else res = await fetch(endpoint, { method: 'GET', headers: this._headers });
            //console.log('resJson=', resJson);
            return res.json();
        } catch (e) {
            console.error(e);
        }
    }

    async batchUpdateProducts(data: Json) {
        //console.log('batchUpdateJsonStr = ', data); // Debug
        //console.log('batch_url = ', this._pMap.wp_batch_update_product_api_url); // Debug
        const res = await this._callWpApi(
            this._pMap.wp_batch_update_product_api_url,
            HttpRequestMethod.post,
            JSON.stringify(data),
        );
        //console.log('Updating Products:', res); //Debug
        console.log('This batch products update completed.');
        return res;
    }

    async createProducts(data: string) {
        return await this._callWpApi(this._pMap.wp_create_product_api_url, HttpRequestMethod.post, data);
        //print('create product', dataSource, '-', r.json()['sku'], '-', r)
    }

    async searchCategoryId(categoryName: string) {
        //console.log('category = ', categoryName); //Debug
        const resJson = await this._callWpApi(
            this._pMap.wp_search_category_api_url + '?name=' + categoryName,
            HttpRequestMethod.get,
        );
        let categoryIds = [''];
        //console.log('resJson(search) = ', resJson); //Debug
        for (let i in resJson) {
            categoryIds.push(resJson[i]['id']);
            return categoryIds;
        }
    }

    async checkIfProductExist(sku: string) {
        //console.log('SKU =', sku); // Debug
        const resJson = await this._callWpApi(
            this._pMap.wp_create_product_api_url + '?sku=' + sku,
            HttpRequestMethod.get,
        );
        //console.log('resJson(check) = ', resJson); //Debug

        // API return status 400 when product does not exsist.
        if (resJson.hasOwnProperty('data') && resJson.data.hasOwnProperty('status') && resJson.data.status == 400)
            return undefined;
        else if (resJson[0] === undefined) return undefined;
        else return resJson[0].id;
    }

    async getProductCategories(category: string) {
        const categoryIds = await this.searchCategoryId(category);
        let ids: [{}] = [{ id: 0 }];
        if (categoryIds !== undefined) {
            ids.pop();
            for (let id of categoryIds) {
                ids.push({ id: id });
            }
        }
        return ids;
    }
}
