#!/bin/sh
readonly name=homeDepotExtractor
readonly runtime=nodejs10
readonly trigger=home-depot-crawler-start
readonly region=us-east4

cp package.json ./dist
cp ./src/config.json ./dist
pushd ./dist
gcloud functions deploy $name --runtime $runtime --trigger-topic $trigger --region $region
popd
